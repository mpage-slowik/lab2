package mpageslowik.com.loancalculator;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText amount, interestRate, numYears;
    private TextView monthPay, totPay, totInt;
    private int numberOfYears;
    private  double yearlyInterestRate, loanAmount;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void calculate(View view){

        amount = (EditText) findViewById(R.id.amount);
        interestRate = (EditText) findViewById(R.id.year_int_rate);
        numYears = (EditText) findViewById(R.id.term);

        loanAmount = Double.parseDouble(amount.getText().toString());
        yearlyInterestRate = Double.parseDouble(interestRate.getText().toString());
        numberOfYears = Integer.parseInt(numYears.getText().toString());

        monthPay= (TextView) findViewById(R.id.month_pay);
        totPay= (TextView) findViewById(R.id.total_pay);
        totInt= (TextView) findViewById(R.id.total_int);

        monthPay.setVisibility(TextView.VISIBLE);
        totPay.setVisibility(TextView.VISIBLE);
        totInt.setVisibility(TextView.VISIBLE);

        monthPay.setText(String.valueOf((double)Math.round(getMonthlyPayment() * 100) / 100));
        totPay.setText(String.valueOf((double) Math.round(getTotalCostOfLoan() * 100) / 100));
        totInt.setText(String.valueOf((double)Math.round(getTotalInterest() * 100) / 100));
    }
    public void clear(View view){
        amount.setText("");
        interestRate.setText("");
        numYears.setText("");
        monthPay.setVisibility(TextView.INVISIBLE);
        totPay.setVisibility(TextView.INVISIBLE);
        totInt.setVisibility(TextView.INVISIBLE);

    }
    private double getMonthlyPayment ()
    {
        double monthlyPayment;
        double monthlyInterestRate;
        int numberOfPayments;
        if (numberOfYears != 0 && yearlyInterestRate != 0)
        {
            //calculate the monthly payment
            monthlyInterestRate = yearlyInterestRate / 1200;
            numberOfPayments = numberOfYears * 12;

            monthlyPayment =
                    (loanAmount * monthlyInterestRate) /
                            (1 - (1 / Math.pow ((1 + monthlyInterestRate), numberOfPayments)));

            monthlyPayment = Math.round (monthlyPayment * 100) / 100.0;
        }
        else
            monthlyPayment = 0;
        return monthlyPayment;
    }
    private double getTotalInterest ()
    {
        return getTotalCostOfLoan() - loanAmount;
    }
    private double getTotalCostOfLoan ()
    {
        return getMonthlyPayment() * numberOfYears * 12;

    }

}
